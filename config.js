/////////////////////////////////////////////////////////////////////////////////////////////
//
// config
//
//    Library for reading and writing config files.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error =    require("error");
var host =     require("host");
var event =    require("event");
var io =       require("io");
var ioAccess = require("io-access");
var ioVolume = require("io-volume");
var fs =       require("io-driver-filesystem");
var ls =       require("io-driver-localstorage");

var config;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Config Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Config() {
    var self = this;

    //
    // constants
    //
    var PROTOCOL_CONFIGURATION = "cfg";
    var PATH_CONFIG_DEVICE_LINUX = "/etc/opt/";
    var PATH_CONFIG_DEVICE_WINDOWS = typeof process != "undefined"? process.env.ProgramData + "\\" : null;
    var PATH_CONFIG_DEVICE_MACOS = "/Library/Preferences/";
    var PATH_CONFIG_USER_LINUX = typeof process != "undefined"? process.env.HOME + "/.config/" : null;
    var PATH_CONFIG_USER_WINDOWS = typeof process != "undefined"? process.env.APPDATA + "\\" : null;
    var PATH_CONFIG_USER_MACOS = typeof process != "undefined"? process.env.HOME + "/Library/Preferences/" : null;
    this.MAX_SIZE = ls && ls.MAX_SIZE? ls.MAX_SIZE : "5242880";

    //
    // private
    //
    var volume;
    var ConfigurationVolume = function(mod, root) {
        this.id = "config";

        this.name = "Configuration (Local)";
        this.protocol = PROTOCOL_CONFIGURATION;
        this.description = "Contains app configuration data.";
        this.size = self.MAX_SIZE;
        this.readOnly = false;

        this.open = function(path, opt_access, create_path) {
            return mod.open(root + (path.indexOf("/") == 0? path.substr(1) : path), opt_access, create_path);
        };

        this.exists = function(path) {
            return mod.exists(root + (path.indexOf("/") == 0? path.substr(1) : path));
        };

        this.query = function(path) {
            return mod.query(root + (path.indexOf("/") == 0? path.substr(1) : path));
        };

        this.events = new event.Emitter(this);
    };
    ConfigurationVolume.prototype = ioVolume;

    function mountConfigVolume(resolve, reject) {
        //mount config volume if not already mounted
        if (!volume) {
            return tryFileSystem().then(resolve).catch(function(e) {
                if (e) {
                    reject(new Error(e));
                }
                return tryLocalStorage().then(resolve).catch(function(e) {
                    reject(new Error(e || "The runtime does not support saving local configuration."));
                })
            });
        }
        else {
            resolve();
        }
    }
    function tryFileSystem() {
        return new Promise(function(resolve, reject) {
            // firstly try file system
            if (fs) {
                var path;
                switch(host.platform) {
                    case host.PLATFORM_MACOS:
                        path = PATH_CONFIG_USER_MACOS;
                        break;
                    case host.PLATFORM_WINDOWS:
                        path = "/" + PATH_CONFIG_USER_WINDOWS;
                        break;
                }
                if (host.isPlatformLinuxFamily()) {
                    path = PATH_CONFIG_USER_LINUX;
                }
                return fs.exists(path).then(function() {
                    volume = new ConfigurationVolume(fs, path);
                    resolve();
                }).catch(reject);
            }
            else {
                reject();
            }
        });
    }
    function tryLocalStorage() {
        return new Promise(function(resolve, reject) {
            // secondly try local storage
            return fs.exists("/").then(function() {
                volume = new ConfigurationVolume(ls, "ls:///");
                resolve();
            }).catch(function() {
                reject(new Error(host.ERROR_RUNTIME_NOT_SUPPORTED, "The runtime does not support saving local configuraration"));
            });
        });
    }

    //
    // public
    //
    this.load = function(path) {
        return new Promise(function(resolve, reject) {
            function success() {
                // load file from volume (if not exist, return blanco object)
                return volume.open(path, ioAccess.READ).then(function(stream) {
                    return stream.readAsJSON().then(function(obj) {
                        return stream.close()
                        .then(function() {
                            resolve(obj);
                        })
                        .catch(reject);
                    }).catch(function(e) {
                        return stream.close()
                        .then(function() {
                            reject(e);
                        })
                        .catch(reject);
                    });
                }).catch(reject);
            }

            if (!volume) {
                mountConfigVolume(success, reject);
            }
            else {
                success();
            }
        });
    };

    this.save = function(obj, path) {
        return new Promise(function(resolve, reject) {
            function success() {
                // save file to volume (and create folders)
                return volume.open(path, ioAccess.OVERWRITE, true).then(function(stream) {
                    var data = JSON.stringify(obj);
                    if (data.length > self.MAX_SIZE) {
                        reject(new Error(io.ERROR_FILE_SIZE_EXEEDS_LIMIT, "The configuration file is too big. There is a size limit of " + self.MAX_SIZE + " bytes per file for storing local configuration data."));
                    }
                    else {
                        return stream.write(data).then(function() {
                            return stream.close()
                            .then(function() {
                                resolve();
                            })
                            .catch(reject);
                        }).catch(function(e) {
                            return stream.close()
                            .then(function() {
                                reject(e);
                            })
                            .catch(reject);
                        });
                    }
                }).catch(reject);
            }

            if (!path) {
                reject(new Error(self.ERROR_INVALID_PATH, "There is no path specified to save the config."));
            }
            else if (!volume) {
                mountConfigVolume(success, reject);
            }
            else {
                success();
            }
        });
    };

    this.getVolume = function() {
        return new Promise(function(resolve, reject) {
            function success() {
                resolve(volume);
            }

            if (!volume) {
                mountConfigVolume(success, reject);
            }
            else {
                success();
            }
        });
    };
}
Config.prototype.ERROR_INVALID_PATH = "Invalid Path";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Singleton Instance
//
/////////////////////////////////////////////////////////////////////////////////////////////
config = new Config();

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = config;